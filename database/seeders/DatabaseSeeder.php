<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        //     'password'=> '123123'
        // ]);

        DB::table('users')->delete();
        DB::table('groups')->delete();

        User::create([
            'name' => 'Pedrito Admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('123123asd'),
            'rol' => 'Admin'
        ]);

       User::create([
            'name' => 'Juancito tecnic',
            'email' => 'tecnico@example.com',
            'password' => Hash::make('123123asd'),
            'rol' => 'Personal'
        ]);

       User::create([
            'name' => 'Ricky',
            'email' => 'cliente@example.com',
            'password' => Hash::make('123123asd'),
            'rol' => 'Cliente'
        ]);

       Group::create([
            'name' => 'Grupo 1'
        ]);

        Group::create([
            'name' => 'Grupo 2'
        ]);

    }
}
