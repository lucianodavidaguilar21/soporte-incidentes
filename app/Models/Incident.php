<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'description', 'assigned_group_id', 'status', 'modified_by'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'assigned_user_id');
    }


  
}
