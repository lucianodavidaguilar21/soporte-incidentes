<?php

namespace App\Http\Controllers;
use App\Models\Comment;
use App\Models\Group;
use App\Models\IncidentComment;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

use App\Models\Incident;
class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    public function showRegisterView(){
        return view('registrar');
    }

    public function showList(){
       
        return view('lista');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   

      $incident = new Incident();
 
      $incident->title = $request->input('title');
      $incident->status = 'Nuevo';
      $incident->description = $request->input('descripcion');
      $incident->save();
   
        return redirect('lista');
       
       

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        
    }

    public function todos(){
        $incidents = Incident::all();
        $grupos = Group::all();
        $users = User::all();
        return view('lista')->with(['incidents'=>$incidents,'groups'=>$grupos, 'users'=>$users]);
         
    }

    public function filtrarByGroup(Request $req){
        if($req->input('grupo') == 'todos'){
            return $this->todos();
        }
        else{
            $incidents = Incident::all()->where('assigned_group_id',$req->input('grupo'));
            $grupos = Group::all();
            $users = User::all();
            return view('lista')->with(['incidents'=>$incidents,'groups'=>$grupos,'users'=>$users]);
        } 
    }

    public function filtrarByStatus(Request $req){
        if($req->input('estado') == 'todos'){
            $incidents = Incident::all();
            return redirect('lista');
        }
        else{
            $incidents = Incident::all()->where('status',$req->input('estado'));
            $grupos = Group::all();
            $users = User::all();
            return view('lista')->with(['incidents'=>$incidents,'groups'=>$grupos,'users'=>$users]);
        } 
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

  

    public function addComment(Request $req){

        $comment = new Comment();
        $comment->comentario = $req->input('coment');
        $comment->incidente_id = $req->input('incident');
        $comment->save();
        return redirect('detalles/'.$req->input('incident'));
    }

    public function showDetails(string $id){
        
        $incidet = Incident::find($id);
        $comments = Comment::where('incidente_id',$id)->get();;
        $user = User::find($incidet->updated_by);
        $groups = Group::all();
         return view('detalles',['incident'=> $incidet,'comments'=>$comments,'user'=>$user,'groups'=>$groups]);
   
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $req, string $id)
    {
 
    }

    public function editarStatus(Request $req, string $id)
    {
        $incident = Incident::find($id);
  
        $incident->status = $req->input('estado');
        $incident->updated_by = $req->input('user');
        $incident->save();
      
        return redirect('detalles/'.$id);
    }


    public function asignar(Request $req, string $id){
        $incident = Incident::find($id);
    
        $incident->assigned_group_id = $req->input('group');
        $incident->status = "Asignado";
        $incident->updated_by = $req->input('user');
        $incident->save();
      
        return redirect('detalles/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
