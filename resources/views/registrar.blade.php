@extends('layouts.app')

@section('content')
<div class="container">
<h3>Registrar nuevo incidente</h3>

@if (auth()->check())
<form action="registrar" method="post">
    @csrf
<div class="form-group mb-3 w-25">
        <label for="title" class="form-label">Título</label>
        <input type="text" class="form-control" id="title" name="title" required>
    </div>
    <div class="form-group mb-3 w-25">
        <label for="descripcion" class="form-label">Descripción</label>
        <textarea class="form-control" id="descripcion"  name="descripcion" required></textarea>
    </div>

  

    <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
</form>
@else

<h3>Debe iniciar sesion</h3>
@endif
</div>

@endsection