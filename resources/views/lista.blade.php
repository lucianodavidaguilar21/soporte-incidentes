@extends('layouts.app')

@section('content')
<div class="container">
  <div style="display:flex; justify-content: space-between;">
    <h1 class="fw-bold">Incidentes</h1>

    <div>
      <span>Filtrar por estado:</span>
      <form style="display:flex" action="/byStatus" method="post">
      @csrf
        <select class="form-select" aria-label="Default select example" name="estado">
          <option selected value="todos">Todos</option>
          <option value="Nuevo">Nuevo</option>
          <option value="Asignado">Asignado</option>
          <option value="Resuelto">Resuelto</option>
        </select>
        <button class="btn btn-success">Filtrar</button>
      </form>
  </div>
  <div>
      <span>Filtrar por grupo:</span>
      <form style="display:flex" action="/lista" method="post">
      @csrf
        <select class="form-select" aria-label="Default select example" name="grupo">
          <option selected value="todos">Todos</option>
          @foreach ($groups as $grupo)
          <option value="{{$grupo->id}}">{{$grupo->name}}</option>
          @endforeach
        
        </select>
        <button class="btn btn-success">Filtrar</button>
      </form>
  </div>
  </div>
  <br>
    <table class="table table-secondary table-striped">
  <thead>
    <tr>
      <th scope="col">Incidente</th>
      <th scope="col">Estado</th>
      <th scope="col">Grupo</th>
      <th scope="col">Modificado por</th>
      <th scope="col">Detalles</th>
    </tr>
  </thead>
  <tbody class="table-group-divider">
   @foreach ($incidents as $incident)
      <tr>
        <td>{{$incident->title}}</td>
        <td>{{$incident->status}}</td>

        @if ($incident->assigned_group_id)
          <td>{{$incident->assigned_group_id}}</td>
        @else
          <td>Sin grupo</td>
        @endif
        @if ($incident->updated_by)
            @foreach ($users as $user)
              @if($user->id == $incident->updated_by)
                <td>{{$user->name}}</td>
              @endif
            @endforeach
        @else
          <td>Sin modificar</td>
        @endif
          <td>
          <a href="/detalles/{{$incident->id}}" class="btn btn-sm btn-primary">Detalles</a>
        </td>
      </tr>

   @endforeach
  </tbody>
</table>
</div>
@endsection