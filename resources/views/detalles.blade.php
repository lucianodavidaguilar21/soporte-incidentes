@extends('layouts.app')

@section('content')
<div class="container">
<h1 >{{$incident->title}}</h1>

<hr class="border border-primary border-2 opacity-25 w-25">

<h4 class="fw-bold">Descripción: <span class="descripcion datos fst-normal text-secondary">{{$incident->description}}</span></h4>

<hr class="border border-primary border-2 opacity-25">


<div class="w-25">
    <h4 class="fw-bold ">Estado: <span class="datos text-secondary">{{$incident->status}}</span></h4>
    <form style="display:flex; align-items:center;" action="/editar/{{$incident->id}}" method="post"> 
         @csrf
            <select class="form-select form-select-sm" aria-label="Default select example" name="estado">
                <option selected>Seleccionar estado</option>
                <option value="Asignado">Asignado</option>
                <option value="Resuelto">Resuelto</option>
            </select>
            <input hidden value="{{Auth::user()->id}}" name="user">
            <button type="submit" class="btn btn-light">Editar</button>
        </form>
</div>


<hr class="border border-primary border-2 opacity-25 w-25">
@if ($incident->assigned_group_id)
    <h4 class="fw-bold ">Grupo: <span class="datos text-secondary">{{$incident->assigned_group_id}}</span></h4>
    <hr class="border border-primary border-2 opacity-25 w-25">
@else
    <div class="w-25">
        <h4 class="fw-bold ">Grupo: <span class="datos text-secondary">Sin grupo </span></h4>
         <form style="display:flex;" method="post" action="/asignar/{{$incident->id}}"> 
         @csrf
            <select class="form-select form-select-sm" aria-label="Default select example" name="group">
                <option selected>Seleccionar grupo</option>
                @foreach($groups as $group)
                    <option value="{{$group->id}}">{{$group->name}}</option>
                @endforeach
         
            </select>
            <input hidden value="{{Auth::user()->id}}" name="user">
            <button type="submit" class="btn btn-success">Asignar</button>
        </form>
    </div>
    <hr class="border border-primary border-2 opacity-25 w-25">
@endif

@if ($incident->updated_by)
    <h4 class="fw-bold ">Modificado por: <span class="datos text-secondary">{{$user->name}}</span></h4>
 
@else
    <h4 class="fw-bold ">Modificado por: <span class="datos text-secondary">Sin modificar</span></h4>
    
@endif
<hr class="border border-primary border-2 opacity-25 w-25">
    <h4 class="fw-bold ">Creación: <span class="datos text-secondary">{{$incident->created_at}}</span></h4>
    <hr class="border border-primary border-2 opacity-25 w-25">
@if ($incident->updated_at == $incident->created_at)
    <h4 class="fw-bold ">Fecha de modificación: <span class="datos text-secondary">dd/mm/yy</span></h4>
@else
    <h4 class="fw-bold ">Fecha de modificación: <span class="datos text-secondary">{{$incident->updated_at}}</span></h4>
@endif
<hr class="border border-primary border-2 opacity-25 w-25">

<div class="addComent">
    <form action="/addComment" method="post">
    @csrf
        <textarea class="form-control border border-warning"  id="coment" name="coment" placeholder="Añadir comentario"></textarea>
        <input hidden value="{{$incident->id}}" name="incident">
        <br>
        <button class="btn btn-warning" type="submit" class="btn btn-warning">Agregar</button>
    </form>
</div>

<br>
<h4 class="fw-bold ">Comentarios</h4>
    
        @if (count($comments)>0)
            @foreach($comments as $comment)
                <!-- @if ($comment->incidente_id == $incident->id) -->
                    <div class="comentario">
                        {{$comment->comentario}}
                        <br>
                        {{$comment->created_at}}
                    </div>
                <!-- @endif -->
            @endforeach
        @else
            Aún no hay comentarios
        @endif
       
</div>
@endsection