@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header fs-3">{{ __('Registro y seguimiento de incidentes') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <a href="/lista" class="btn btn-primary btn-lg">Lista</a>

                   <a href="/registrar" class="btn btn-primary btn-lg">Registrar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
