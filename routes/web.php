<?php

use App\Http\Controllers\ListarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/home');
});

Route::get('/lista','App\Http\Controllers\IncidentController@todos');

Route::post('/lista','App\Http\Controllers\IncidentController@filtrarByGroup');

Route::post('/byStatus','App\Http\Controllers\IncidentController@filtrarByStatus');

Route::get('/registrar','App\Http\Controllers\IncidentController@showRegisterView');

Route::post('registrar','App\Http\Controllers\IncidentController@store');

Route::get('/detalles/{id}','App\Http\Controllers\IncidentController@showDetails');

Route::post('/asignar/{id}','App\Http\Controllers\IncidentController@asignar');

Route::post('/editar/{id}','App\Http\Controllers\IncidentController@editarStatus');

Route::post('/addComment','App\Http\Controllers\IncidentController@addComment');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
